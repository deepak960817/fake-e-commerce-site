import React, {Component} from 'react';
import Card from 'react-bootstrap/Card';
import {connect} from 'react-redux'

const mapStateToProps = ((state) => {
    return{
        product : state.product.selectedProduct,
        productId : state.productID.id,
    }
})


class ProductDetail extends Component {

    componentDidUpdate = (prevProps, prevState) => {
        if(prevState.props.productId !== this.props.productId)
        {
            this.fetchProductDetail()
        }
    }

    render() { 
        // console.log(this.props.productId)
        // console.log(this.props.product)
        return ( this.props.product === [] ? <h1>Loading...</h1> :
            <React.Fragment >
            {
                [this.props.product].map((product) => {
                return(   
                    <Card key={product.id} style={{ width: '30rem'}}>
                    <Card.Body>
                    <Card.Img variant="top" style={{height:'20rem'}} src={product.image} />
                        <Card.Title>{product.title}</Card.Title>
                        <Card.Text>
                            <p><b>Price: </b>{product.price}</p>
                            <p><b>Description: </b>{product.description}</p>
                            <p><b>Rating: </b>{product.rating.rate}</p>
                        </Card.Text>
                    </Card.Body>
                    </Card>
                    )
                })
            }
        </React.Fragment> 
        );
    }
}
 
export default connect(mapStateToProps)(ProductDetail);


