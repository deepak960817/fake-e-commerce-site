import React from 'react';
import {connect} from 'react-redux'
import Card from 'react-bootstrap/Card';
import {Link} from 'react-router-dom'
import * as actions from '../redux/actions/productActions'

const mapStateToProps = ((state) => {
    return{
        products : state.allProducts.products,
    }
})

const mapDispatchToProps = (dispatch => {
    return{
        setPath: (id) => dispatch(actions.setProductPath(id)),
        setProduct: (product) => dispatch(actions.selectedProducts(product))
    }
});

const ProductComponent = (props) => {

    // function handlesetPath(id, product){
    //     props.setPath(id);
    //     props.setProduct(product)
    // }

    function handleClick(id, product) {
        props.setPath(id);
        props.setProduct(product)
    }

    return ( 
        <React.Fragment >
            {
                props.products.map((product) => {
                return(
                        <Link key={product.id} to={`product/${product.id}`}>
                        <Card onClick={() => handleClick(product.id, product)} style={{ width: '18rem'}}>
                        <Card.Body>
                        <Card.Img variant="top" style={{height:'20rem'}} src={product.image} />
                            <Card.Title>{product.title}</Card.Title>
                            <Card.Text>
                                <p><b>Price: </b>{product.price}</p>
                            </Card.Text>
                        </Card.Body>
                        </Card>
                        </Link>
                )
            })
        }
        </React.Fragment>
     );
}
 
export default connect(mapStateToProps, mapDispatchToProps)(ProductComponent);