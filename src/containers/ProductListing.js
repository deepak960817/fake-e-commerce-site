import React, {Component} from 'react';
import {connect} from 'react-redux'
//import { mapDispatchToProps, mapStateToProps } from 'react-redux';
import axios from 'axios'
import ProductComponent from './ProductComponent';
import * as actions from '../redux/actions/productActions'


const mapStateToProps = ((state) => {
    return{
        fetchProducts : state,
    }
})

const mapDispatchToProps = (dispatch => {
    return{
        storeData: (data) => dispatch(actions.setProducts(data))
    }
});


class ProductListing extends Component {

    fetchProducts = async () => {
        const response = await axios.get('https://fakestoreapi.com/products').catch(err => {
            console.log("Error", err)
        })
        this.props.storeData(response.data);
    }

    componentDidMount = () => {
        this.fetchProducts()
    }
       
    render() { 
        // console.log("Products:", this.props.fetchProducts)
        return (
            <div style={{backgroundColor:'white', display:'flex', justifyContent:'center', flexWrap:'wrap', gap:'2rem'}}>
                <ProductComponent />
            </div>
        );
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(ProductListing);
