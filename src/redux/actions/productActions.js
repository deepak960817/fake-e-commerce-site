import {ActionTypes} from '../constants/action-types'

export const setProducts = (products) => {
    return {
        type: ActionTypes.SET_PRODUCTS,
        payload: products,
    }
}

export const selectedProducts = (product) => {
    return {
        type: ActionTypes.SLELCTED_PRODUCT,
        payload: product,
    }
}

export const removeSelectedProducts = (product) => {
    return {
        type: ActionTypes.REMOEVE_SELECTED_PRODUCT,
        payload: product,
    }
}

export const setProductPath = (path) => {
    return{
        type: ActionTypes.SET_PRODUCTID_PATH,
        payload: path
    }
}