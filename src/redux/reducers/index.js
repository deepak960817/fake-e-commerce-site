import { combineReducers } from "redux";
import * as actionReducers from "./productReducer";

const reducers = combineReducers({
    allProducts: actionReducers.productReducer,
    productID: actionReducers.ProductIDReducer,
    product: actionReducers.selectedProductReducer
})

export default reducers;