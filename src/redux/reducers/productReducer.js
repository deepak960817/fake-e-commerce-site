import {ActionTypes} from '../constants/action-types'

const initialState = {
    products: [],
    selectedProduct: [],
    id: ""
}

export const productReducer = (state = initialState, {type, payload}) => {
    switch(type) {
        case ActionTypes.SET_PRODUCTS:
            return {...state, products: payload}
        default:
            return state
    }
}

export const ProductIDReducer = (state = initialState, {type, payload}) => {
    switch(type) {
        case ActionTypes.SET_PRODUCTID_PATH:
            return {...state, id: payload}
        default:
            return state
    }
}

export const selectedProductReducer = (state = initialState, {type, payload}) => {
    switch(type) {
        case ActionTypes.SLELCTED_PRODUCT:
            return {...state, selectedProduct: payload}
        default:
            return state
    }
}